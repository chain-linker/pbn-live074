---
layout: post
title: "[주식 공부] 나스닥 중국 소형주 IPO 중단, IPO란?"
toc: true
---


## [주식 공부] 나스닥 중국 소형주 IPO 중단, IPO란?
 

 

  소규모 중국 기업이 나스닥에 상장한 추후 폭등, 폭락을 반복하면서 나스닥은 중국 소규모 기업의 IPO를 중단하고 승인을 보류했습니다.
  앞서 말한 소식 자세한 내용과 IPO의 뜻을 알아보았습니다.
 

 

 

 

 

 

### 1️⃣ IPO의 뜻
 

  Initial Public Offering의 약자로, 기업이 일정 목적을 가지고 자사의 주식과 경영내용을 공개하는 것으로, 일정 규모의 기업이 상장절차 등을 밟기 위해 행하는 작은계집 투자자들에 대한 첫 주식공매를 말합니다.
  공모한 주식의 매매 거래가 활발하게 이뤄지도록 '증권거래소 상장'이라는 수단을 이용하게 되며, 원칙적으로 기업공개와 상장은 같은 개념은 아니고 기업의 공개를 원활히 염절 위해 상장의 수단을 이용하게 됩니다.
  IPO를 통해 기업은 자금조달을 원활히 하고, 재무구조를 개선하며, 나아가 국민의 기업참여를 활발히 하고, 인민 경제 발전에 기여하기 위한 목적으로 이뤄집니다.
 

 

### 2️⃣ 나스닥, 폭등과 폭락을 반복한 중국 소규모 회사 윗사람 사례로 인해 중국 소규모 기업체 IPO 중단, 승낙 보류
 

  1. [주식](https://lunchfar.com/life/post-00041.html) 홍콩에 기반을 둔 AMTD Digital Inc.는 IPO 첫날 152% 오른 이후 7월 중순부터 8월 초까지 15,000% 급등했으나, 후 98% 하락했습니다.
  2. 중국 피복 제조업체인 Addentax Group은 8월 31일 IPO 이후로 9,000% 급등했으나, 이강 99.6% 하락했습니다.
  3. 홍콩에 기반을 둔 금융 덤 회사인 Magic Empire Global은 IPO 첫날 2,000% 소망 높은 가격에 마감한 후 97% 하락했습니다.
  4. 이러한 사건들로 인해 나스닥은 중국 소규모 기업의 IPO 허가 서한을 연기하고 운행 관계 당사자에 대한 수확 정보를 요청하고 있으며, 최종 공지가 있을 때까지 IPO 승인이 중단된다고 말했습니다.
 

 

 

 

 

 

 시고로 정보는 어떠신가요? ⏬⏬⏬
 

 2022.10.24 - [주식/주식공부] - 미국 주식 공부 - 매수 신호, 시장이 바닥임을 알려주는 잣대 CBOE VIX 지수
 2022.10.23 - [주식/주식공부] - 미국 주식 아부 - 골드만 삭스 변리 얼굴빛 행동 포트폴리오
 2022.10.23 - [주식/주식공부] - 주식 관해 - YoY, QoQ, YTD, %p란?
 

 

 관련 링크
 

 [네이버 지식백과 시사상식사전 - 기업공개] https://terms.naver.com/entry.naver?docId=66686&cid=43667&categoryId=43667
 [똑똑 - 주식 투자할 때, IPO 뜻은?] https://www.dokdok.co/brief/ipo
 [Seeking Alpha - 나스닥, 금새 추락한 중국 소형주 IPO 중단] https://seekingalpha.com/news/3893899-nasdaq-is-said-to-halt-small-cap-chinese-ipos-after-price-spikes
 

 괜찮았다면 ❤ 공감, 댓글 남겨주세요.
 궁금한 점 언제나 환영합니다.
 개선사항, 불편한 점도 백날 환영합니다.
 댓글 달아주세요!
 

 그럼 이만!
 BYE!
