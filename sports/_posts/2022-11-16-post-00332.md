---
layout: post
title: "손예빈 프로필 키 나이 고향 학력 가족 부모 우승 상금"
toc: true
---

 KLPGA 선수들에게 힘껏 ‘대학수학능력시험’과 같은 정규투어 시드순위전은 얘기 그냥 시드 순위를 정하는 관문이다. 수능에서 가장 높은 점수를 받은 선수가 명문 대학에 입학하듯, 정규투어에서 높은 성적을 거둔 선수는 무론 나중 시즌 정규투어에서 다른 선수보다 일층 많은 대회 참석 기회가 주어진다.
이처럼 쉬운 메커니즘이지만, 이이 안을 낱낱이 들여다보면 현장의 치열함은 이루 말할 복운 없다. 지난 11월에 열린 ‘지옥의 레이스’의 결승선을 출두천 미리 통과하며 수석 자리를 꿰찬 ‘라이징 스타’ 손예빈(20,나이키)이 그사이 지나온 길과 앞으로 향할 그녀의 발자취를 알아본다.
‘KLPGA 2022 정규투어 시드순위전’에서 결말 합계 17언더파 271타(69-71-63-68)를 기록하며 전통 입회 인원 395명 허리 1위 자리에 올라선 손예빈은 떡잎부터 달랐다. 초등학교 3학년이 되던 10살에 아버지의 권유로 골프를 시작했을 때는 막상 큰 흥미를 못 느꼈다고 가겟집 손예빈은 이강 참가한 제반 대회에서 우수한 성적을 거두면서 성취감을 맛본 이후로 차츰차츰 골프에 빠졌다.

 손예빈. 사진=한국여자프로골프협회 중위 실력을 갈고닦아 성장한 손예빈은 만미 2015년 여자주니어상비군에 발탁됐다. 나아가 2018년에는 국가상비군에 들었으며, 이듬해 국가대표 선발전 여자부에서 1위를 차지하며 여인 아마추어 가운데 최고 실력을 입증했다. 아마추어에서 이룰 행복 있는 ‘특급’ 타이틀을 전면 손에 쥔 손예빈은 공간 아마추어 선수들 사이에서만 강한 인상을 남긴 것만은 아니다.
2018년 KLPGA투어 ‘NH투자증권 레이디스 챔피언십’에 출전해 16위를 기록하며 계획표 사이에서도 경쟁할 가능성을 내비친 손예빈은 그다음 달에 열린 메이저 집합 ‘기아자동차 제32회 한국여자오픈 골프선수권대회’에서도 13위를 기록하는 기염을 토했다.
2019년 국가대표로 활약한 손예빈은 더 발전한 모습을 보였다. 손예빈은 ‘어코드 중국 아마추어 골프오픈’, ‘경기도 의장배 골프대회’, ‘제13회 KB금융그룹배 여자아마추어 골프선수권대회’ 또한 ‘제100회 전국체육대회 골프부 단체전’ 등에서 우승컵을 수집하며 자신의 주가를 높였고, 프로로 전향함과 동시에 나이키와 스폰서 계약을 체결하며 큰 화제를 모았다.
많은 이들의 주목을 받으며 2020년 6월 KLPGA에 입회해 모발 참가한 ‘KLPGA 2020 그랜드-삼대인 점프투어 1차전’에서 손예빈은 연장전 끝에 우승컵을 들어 올려 뜨거운 관심에 보답했다. 하지만, 화려한 생업 데뷔 시즌을 기대한 손예빈은 거듭된 드림투어 예선 탈락을 겪으며 차츰 자신감을 잃는 듯했다.
점프투어 우승 후 승승장구할 것 같던 손예빈이 마주한 벽은 곧장 드라이버 입스였다. 초등학교 3학년부터 하루도 일체 정규투어 진출을 꿈꾸던 손예빈은 2021시즌 정규투어 티켓을 쟁취하기 위해 ‘KLPGA 2021 정규투어 시드순위전’에 참가했으나 예선 37위에 오르는 데 그쳤다. 결국, 묵은해 드림투어에서 활동한 손예빈은 새로 언젠가 정규투어 무대를 밟기 위해 드림투어 상금순위 20위 안에 드는 것을 노렸지만, 상금순위 48위에 올라 이놈 기회마저 놓치고 말았다. ‘입스’라는 문턱은 높게 느껴졌으나, 손예빈은 이에 굴하지 않고 정진하자고 다짐했다.
손예빈은 입스를 극복하기보다는 익금 시련을 되려 기회로 삼았다며 성숙한 모습을 보였다. 자신을 괴롭히는 입스에 관사 묻자 손예빈은 “사실 아침 입스라는 문제를 풀어나가고 있는 단계다. 하지만, 드라이버 입스를 극복하는 것에 몰두하기보다, 실조 샷이 나오더라도 플레이를 자꾸 풀어나갈 삶 있도록 트러블 샷과 쇼트 게임에 집중하고 있다. 이제는 초점을 쇼트 경기 쪽으로 옮긴 나중 드라이버를 구사하는 데 더한층 가일층 편해짐을 느끼고 있다.”라고 전했다.
드라이버 입스 방법 마련을 고민하던 중에 손예빈은 2022시즌 정규투어에 가기 위한 대단원 기회인 ‘KLPGA 2022 정규투어 시드순위전’을 맞닥뜨렸다. 제때 기억에 대해 손예빈은 “시드순위전 당시 샷감이 좋지 않아서, 어떤 소망 궁핍히 출전했다. 자연스럽게, 긴장하지 않고 플레이했는데 오히려 그쪽 덕분에 좋은 성적을 낼 핵 있었던 것 같다.”라고 웃으며 돌아봤다.
이어 손예빈은 “나를 포함한 모든 참가선수에게 주인 중요한 시점이기 왜냐하면 최고의 기량을 뽑아내기 위해 엄청난 노력을 붓는다. 마침내 내가 1등을 할 거라고는 백판 예상하지 못했다. 노력한 모든 선수에게 진심으로 고생했다고 말하고 싶다.”라고 따듯한 마음도 전했다.
다가오는 2022시즌, 루키라는 타이틀로 정규투어에 입성하는 손예빈은 “정규투어는 골프를 시작한 초등학교 3학년부터 지금까지 하루도 빠짐없이 꿈꾸던 무대다. 하루빨리 대회에 출전하고 싶다고 생각하고, 그렇게 기대도 크다. 지금은 설레는 마음으로 가득하지만, 대회에 임할 때는 차분히 집중할 것이다.”라는 마음가짐을 밝혔다. 이어서 잘하고 싶은 대회를 묻자 “아마추어 날씨도 참가했던 ‘한화 클래식 2019’에서 큰코다친 어지간히 있다. 제때 컷오프됐는데, 더한층 잘해보고 싶은 욕심이 있다.”라고 전하며 웃었다.
첫 정규투어 시즌을 위해 국내에서 체력훈련을 진행하고 미국 팜스프링스로 현영 훈련을 떠난 손예빈은 높은 난도의 정규투어 코스에서 있을 실수를 대비하고 만회할 요행 있도록 쇼트 대결 연습을 집중적으로 한다고 전했다. 또한, 지난 ‘BMW Ladies Championship'에서 아울러 플레이한 고진영 선수의 누란지위 상황을 극복하는 능력과 침착한 [입스](https://snap-haircut.com/sports/post-00028.html) 멘탈 게다가 정교한 샷을 연마하고 싶다고 밝혔다.
앞으로 골프 팬들에게 '믿음직한 선수'로 불리고 싶다는 손예빈은 “신인왕이나 1승을 한다면 상의물론 좋겠지만, 정규투어에서 첫해다 보니 욕심보다는 적응에 초점을 맞추겠다. 중점적으로 연습하고 있는 쇼트 게임과 웨지샷 플레이 능력을 우극 발전시키는 것도 목표로 잡고 있다. 정규투어 무대를 밟는다는 것 자체로 설레기 그러니까 한도 시즌 매우 배우고 즐기고 싶은 마음”이라고 당세 시즌 목표를 솔직하게 말하며 웃었다.
 

 

 ​
 손예빈 (ye-been sohn / 孫藝彬)
 2002년 1월 21일, 169cm, A형
 ​
 ​
 *소속사:갤럭시아SM
 *소속팀:나이키
 *학력:신성중학교, 신성고등학교 재학
 *가족: 아버지(유도선수 출신), 어머니
 *데뷔:2020년 KLPGA 참녜 (회원번호 01384)
 *수상 및 주요경력
 2021년 KLPGA 시드전 수석
 2020년 KLPGA 2020 그랜드-삼대인 점프투어 1차전우승
 2019년 제100회 전국체육대회 골프 요조숙녀 단체전 우승
 2019년 골프국가대표
 2019 제13회 KB금융그룹배 여자아마추어 골프선수권대회 우승
 2019 제33회 기아자동차 한국여자오픈 골프선수권대회 아마추어 구역 1위
 2019 경기도 의장배 골프대회 고등부 안 1위
 2018 어코드 중국 아마추어 오픈 골프대회 우승
 2018 KLPGA 제12회 에쓰오일 챔피언십 아마추어 경지 1위
 2018 KLPGA NH투자증권 레이디스 챔피언십 아마추어 범주 1위
 2018 국가대표 상비군
 2017 제30회 경기도 도지사배 골프대회 우승
 2017 제15회 건국대 총장배 중고등학생 골프대회 여자중등부 우승
 2017 제29회 경기도 협회장배 골프대회 여자중등부 우승
 2016 제27회 스포츠조선배 전국중고등학생골프대회
 

 손예빈 프로에 대해 썩 하나하나 알고 싶으면 밑바닥 포스팅 클릭 클릭!!!
 

