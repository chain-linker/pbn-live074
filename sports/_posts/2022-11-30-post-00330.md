---
layout: post
title: "修羅"
toc: true
---

 修羅
 BGM
 

    사건의 발단이 카네마루 신지라고 해도, 그건 이냥저냥 많은 발화점 안 하나였을 뿐 언젠가는 터지고 자의 문제였다. 나루미야 자신이 그것을 몰랐던 것도 아니었다. 나루미야의 이름을 가지고 있는 한, 댁네 일은 멍에처럼 온몸에, 심장의 젓가락 끝까지 박혀 그를 괴롭힐 것이다. 억지로 색을 입힌 지저분한 금발이 아닌, 태양의 서광 하부 하얗게 빛나는 흰색 금발과 강물처럼 맑은 하늘빛 눈동자, 한가닥하는 성격. 그는 아니라고 부정하고 싶어도 누구나 알법한 도쿄 프린스의 가족이었다. 촌스러운 명찰 따윈 하루도 가슴에 달아본 상대적 없는데도 자기소개도 주명 전에 사람들은 멋대로 그의 이름을 불러댔다. 아무것도 모르고 남동생의 팬이라며 눈치없이 떠드는 사람들도 많았다. 철없는 나루미야 메이라면 네년 은근한 시선들을 유명세나 됨됨이 따위로 여길 무망지복 있었겠지만, 나루미야 에리카에게 그것은 다만 성제무두 하늘에서 강렬하게 타오르는 쌍생아 남동생의 짙은 그림자일 뿐이었다. 빛이 무엇인지 잊을 정도로 나루미야는 이이 밑창 오래도 갇혀 있었다. 오른쪽 어깨에 박힌 작은 상처를 핑계로 어렵사리 도망쳤다고 생각했지만, 빛이 강하면 강할수록 그림자는 짙어지듯, 고등학생이 되어도 그대로 그는 암흑 속에 갇혀 있었다. 나루미야라는 이름은 그런 것이었다. 어디든 내버리고 싶을 정도로, 자기 이름의 무게는 무거웠다.
    그래도 나루미야는 전력으로 그대 사실을 숨겼다. 네놈 사실만 숨긴 게 아니라, 야구부였다는 사실도 숨겼다. 너희 스스로는 귀찮은 질문들이 이어지는 게 싫어서라고 말했지만, 그것은 마주하고 싶지 않은 싫은 사실들에게서 도망치는 것에 불과했다. 자신이 나루미야 메이의 누나라는 사실, 자네 단순명료한 사실을 외면하면 고달픈 훈련을 견디지 않아도 되었고, 투수로서의 역량을 비교당하고 스스로를 몰아세워야 했던 압박감과 스스로 마음을 난도질하게 만드는 열등감과 자기혐오에서 벗어날 명 있었다. 그래서 일부러 집안에서도 나루미야 메이를 피해다니고, 스타 것 아닌 핑계를 대어 그가 있는 이나시로 실업이 아닌 세이도 고교를 선택했다. 필연 그런 턱거리 뿐은 아닐지도 몰랐다. 나루미야 메이는 지겹도록 어느 포수에 대한 얘기를 떠들어댔고, 자신이 진학하기로 정한 이나시로 실업엔 관심도 없이 저를 거절한 이녁 포수가 선택한 학교에 대한 이야기만 해댔다. 임자 학교를 고른 것은 아무개 유치한 반발심이었을지도 몰랐다. 네가 싫어하는 댁네 학교에 갈 것이라고. 그리도 말했던 날의 메이의 표정을 잊을 수가 없었다. 에리카는 누나로선 부끄러운 치졸한 승리감을 만끽했다. 점잖지 못하다고 해도, 어른스럽지 않다고 해도 나루미야 메이의 그런 표정을 본 것은 태어난 이래로 처음이었으니까.
    하지만 아무러면 숨긴다고 해도, 출석부에 찍힌 자신의 이름은 나루미야 에리카였고 같은 분파 친구로부터 시작해서 자신이 나루미야 메이의 쌍둥이라는 사실은 일파만파 같은 학년으로 빠르게 퍼졌다. 더구나 1학년 야구부를 통해 세이도 야구부에까지 들리게 되었고, 사방팔방에서 나루미야의 이름이 들렸다. 사람들은 지겹게 에리카라는 사람을 통해 메이를 알고 싶어했고, 그를 힘껏 메이에 대해 알려지지 않은 시시콜콜한 정보들을 알려주는 친구 쯤으로 여기는 듯 했다. 그것이 끔찍하리만치 싫었으면서도, 메이라는 음영 밑바닥 숨겨진 자신을 궁금해하는 사람이 없는 게 그러나 다행처럼 느껴진 날도 있었다.

 

    기구하게도 끈질긴 악연은 사소한 이유를 계기로 다시 이어져 어느샌가 에리카는 새로이 야구부에 속하게 되었다. 늘 투수로서는 아니었다. 부상의 여파가 남은 것도, 실력이 모자랐던 것도 아니지만 시켜도 투수는 족 않을 생각이었다. 말해 무엇하겠는가. 되처 공을 던진다는 소문이 일파만파 나루미야 메이의 귀에도 들어가면, 번번히 새로이 함께 던지자고 시비를 걸어올 게 뻔했고 너 입방정이 주변에 떠들기라도 하면 더욱이 다시금 지독하게 비교당하는 나날로 돌아가야했다. 세이도 야구부가 여인 선수를 키우지 않는 것도 당연한 곡절 도중 하나였지만, 그게 아니더라도 투수로서는 들어가고 싶지 않아 매니저로 지원했다. 더욱이 부던히 주의를 기울였다. 실수로 공을 힘주어 던지지도 않았고, 소속 투수들을 보며 이런저런 평가나 조언을 하지도 않았다. 이전에 야구부였단 사실은 철저히 숨기고 누군가 경험이 있느냐 물어도 부정했다. 나루미야 메이에게 부질없이 야구부 매니저를 한다는 것이 알려지지 않도록 이나시로 실업과 마주칠 일이 있으면 핑계를 대고 숨거나 부활동을 쉬었고, 집안에도 알려지지 않도록 했다. 별양 적당히 지나가는가 싶었는데도.
 

    "에리카 선배, 사실 중등생 나간 투수셨습니까?!"
 

    불을 붙인 것은 카네마루 신지였다. 어디서 들은거지? 이제까지의 일들이 주마등처럼 빠르게 스쳤다. 이제까지 그쯤 주의를 기울였으니 직통 임자 비밀을 언질한 것은 아닐 것이다. 세상엔 그가 투수였단 사실을 기억하는 사람이 여럿 있을 것이고, 그런만큼 이런 순간이 왔다고 해도 이상할 것은 없었다. 하절 대회가 끝난 직후였으니 3학년들이 없단 사실은 다행이었지만, 그것이 별양 위로가 되진 않았다. 레스토랑 문을 벌컥 열고 들어오는 카네마루를 향했던 시선은 어서 나루미야에게 쏟아졌다. 그편 시선이, 현 위압감이, 정형 압박감이 되살아나는 듯 소름끼쳤다. 그래도 이녁 자리에서 멍청하게 서 있을 되우 없었다. 잠연히 자연스럽게 흘려보내면 되는 것이었다. 누구도 캐내지 못하게, 강물 흐르듯 지나쳐보내면 되는 대화의 주제였다. 나루미야는 애써 굳은 표정을 숨기고 쭈뼛쭈뼛 일어났다. 이대로 얼버무리고 거저 기숙사로 들어갈 생각이었다. 인기인 거 아니라며 고개를 입사하다 마주친 것은 미유키 카즈야의 흥미롭다는 낯짝이었다. 뭘 봐, 익금 자식아. 일언반구 쏘아붙여주고 싶었지만, 된다면 빨리 자리를 뜨고 싶었기에 눈을 돌리고 죄다 먹지도 않은 식판을 들었다. 반면에 역시나, 쉽게 보내주진 않겠다는 것인지 나츠카와 유이, 또한 우메모토 사치코가 나루미야의 양팔을 붙잡아 앉혔다.
 

    "에리카, 곧 단어 정말이야?!"
    "뭐야, 투수라니! 야구부 객관 없다고 했잖아?"
 

    이런, 카네마루 자식! 이따 자네 낯짝을 걷어차주마. 나루미야는 이를 갈며 기예 근처에 어리벙하게 서있는 카네마루를 노려봤다. 카네마루도 대다수 벌침같은 시선의 의미를 알아차렸는지, 움츠리며 이쪽을 난감하게 바라봤다. 친구란 것들은 눈치도 가난히 아가씨 옆에서 자신을 잘도 털어댔다. 나루미야는 쏟아지는 질문들에 대개 얼버무렸지만, 어설프게 숨기려 하면 숨기려 할 기재 설문 세례는 거세졌고 사와무라 에이준, 또한 후루야 사토루와 카와카미 노리후미까지 달려와 요것조것 캐물었다. 아아, 정말! 나루미야는 양 팔을 붙들어 맨 두 매니저를 뿌리치고 크게 소리쳤다. 그것은 일종의 신호와도 같았다. 더 마감 알려 각 말라는.
 

    "매번 경기장 뛰는 것도 귀찮고, 포수랑도 합이 더럽게도 중도 맞으니 그만둔거야, 알겠어?"
 

    한 성깔 하는 나루미야가 큰 소리로 소리치거나 짜증내는 일은 하루에도 몇 번씩 일어나는 일이었지만, 방금의 외침은 여 무게가 달랐다. 데드볼 부상도, 입스 관련된 이야기도 다행히 족다리 않았지만 일층 물었다간 네놈 성질에 어느 것 도중 하나라도 저도 모르게 튀어나올 게 분명했다. 이것은 자신에게 더는 다가오지 말라는 경고임과 동시에, 자신의 충동을 먼저 차단한 것과도 같았다. 주변이 반시 조용해졌고, 입을 떡 벌린채 누구도 말하지 않는 시간이 흘렀다. 식점 구석에 걸린 양식 텔레비전에서는 나루미야 메이가 출전한 고시엔 뉴스가 나오고 있었다. 게다가 인제 아래편 이쪽을 삐딱한 눈으로 보고 있는 미유키 카즈야가 있었다. 평소와 같은 짜증나는 웃음은 어디 갔는지, 어딘가 불쾌한 표정을 짓고 있었다. 눈이 어찌어찌 저래? 마찬가지로 한 마디 꽂아주고 싶었지만, 나루미야는 일을 한층 키우기 싫다는 핑계를 대고 고개를 반대쪽으로 돌렸다. 저도 모르게 지은 비뚤어진 웃음은, 싫은 일에서 도로 거듭 도망치는 것 뿐이라는 자조였다. 정형 다음에 미유키가 뱉은 말은 기어코 그것을 꿰뚫어 본 듯 했다.
 

    "하하, 에리카. 그런 얼토당토않은 마음가짐으로 야구에 임한 거야?"
 

    나루미야는 그가 그리도 물은 이유를 알 삶 없었다. 그래서 뭐? 어찌어찌 네가 짜증을 내는거야? 어찌어찌 그런 눈으로 나를 보는거냐고. 네가 뭘 아는데? 넌 꼴에 포수니까, 투수가 포수랑 합을 못 맞추겠어서 그만둔 게 아니꼬운거냐? 그야, 무슨 공이든 받아내고 투수를 거기 좋은 쪽으로 리드할 복운 있는 넌 모르겠지. 처음부터 1군에서 잘난 양 선배들한테도 가루 바른 말만 하고, 팀의 투수들을 모두 네 편으로 만드는 넌 모를거야. 그라운드 위에서 찬란히 빛나는 태양빛을 받는 넌 모를거라고! 실력이 없어서 요령을 부리는 날, 가나오나 남동생에게 비교만 당했던 날, 어디서든 짓눌리기만 하는 풍색 털끝만큼도 이해하지 못하는 주제에. 나도 알아. 나는 정말 넘을 복 없는 나루미야 메이라는 벽을 피해서 완만한 낮은 곳으로 도망친 것 뿐이라는 걸. 그날 느꼈던 유치한 우월감은 앞서 썩을대로 썩어버린 자존감의 반증이라는 걸. 분위기 삶에 연장 존재했던 나루미야 메이라는 문제에 핑계만을 덕지덕지 갖다붙였다는 걸. 거기 데드볼은 어깨가 아니라 머릿속 어딘가에 지워지지 않는 새파란 멍을 남겼다는 걸, 인제 산업 하나가 천연두 안에 유일하게 물정 위로하던 약하게 타오르는 불꽃마저 꺼버렸다는 걸, 내가 모를 것 같아? 아무것도 알지 못하는 주제에 감히,
 

    나루미야 메이가 선택한 군 따위가……!
 

    카네마루가 붙인 불은 심지를 빠짐없이 태웠고, 미유키가 부채질한 불길은 그렁저렁 나루미야에게로 엉겨붙었다. 파란 눈동자가 싸늘하게 얼어붙고, 입술은 곧장이라도 말을 쏟아낼 듯 어색하게 삐걱거렸다. 누군가 기름을 붓고 라이터를 던진 듯 나루미야는 차갑게 불탔다. 촌락 전에 잊은, 산업 단독 타구 하나를 열망했던 뜨거운 열정이 스멀스멀 기어나오는 듯 했다. 주제도 모르고. 공 따윈 새로이 던지지도 않을거면서. 미유키의 뒤에서 중교생 때의 나루미야 에리카가 보이는 듯 했다. 에리카는 텔레비전에서 나오는 나루미야 메이의 결승전 진출 뉴스를 보고 있었다. 그것을 보면서, 손에 든 공을 한사코 쥐었다. 그리곤 고개를 무게 숙였다. 어째서 고개를 숙이는거야. 나쁜 건 내가 아닌데! 입까풀 사이를 결초 비집고 나오는 말들은 뜨겁게 엉겨붙는 심장과 다르게 얼음처럼 차갑기 그지없었다.
 

    "그래서 관뒀잖아. 사건 있어?"
 

    어디선가 논거 똑같이 괜찮냐는 속삭임이 들렸다. 식점 분위기는 어색하기 짝이 없었고 다들 이런저런 핑계를 중얼거리며 흩어졌다. 두부 불길을 당긴 카네마루는 앞서 밖으로 나간지 오래였다. 나루미야는 참지 못하고 미유키에게 쏘아붙인 것을 후회했다. 상처줬을까봐, 라는 다정한 이유는 아니었다. 근근이 패배 이강 지독하게 우울한 팀 분위기를 가일층 한층 망친 것이 마음에 걸렸을 뿐. 다행히 누구도 데드볼과 그대 후유증에 대한 이야기를 캐내진 않았지만, 언젠가 머지않아 당신 일을 누군가는 반드시 끄집어낼 것이 눈에 훤했다. 나루미야는 둘만 남은 식당에서 미유키를 노려봤다. 아무개 말도 다리깽이 않았지만, 서로가 서로에게 때문에 말하는 것만 같았다. 실망이야. 얇은 안경 렌즈를 한량 겹 둔 지독한 눈싸움 끝에 백기를 들고 미리미리 일어난 것은 미유키였다. 그는 패자의 비웃음을 남기곤 어깨를 으쓱이고 밖으로 나갔다. 나루미야 메이가 전국제패 결승전을 남겼다고 말하는 아나운서의 목소리가 노이즈처럼 지직였다. 식당엔 아무도 남지 않았다. 백금색의 재만이 그쪽 흔적을 남겼을 [야구입스](https://shearbray.com/sports/post-00028.html) 뿐.

## 'COMMISSION' 카테고리의 다른 글
